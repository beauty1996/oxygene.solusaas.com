<?php

use Illuminate\Support\Facades\Route;

include('web_builder.php');

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// login2, register2 pages
Route::view('login2', 'auth.login2');
Route::view('login3', 'auth.login3');
Route::view('register2', 'auth.register2');
Route::view('register3', 'auth.register3');

Route::get('/', 'HomeController@dashboard')->middleware('auth')->name('dashboard');

// Route::resource('users', 'UsersController');

// GUI crud builder routes
Route::group(['middleware' => 'auth'], function () {
    Route::get('builder', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@builder')->name('io_generator_builder');

    Route::get('field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@fieldTemplate')->name('io_field_template');

    Route::get('relation_field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@relationFieldTemplate')->name('io_relation_field_template');

    Route::post('generator_builder/generate', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generate')->name('io_generator_builder_generate');

    Route::post('generator_builder/rollback', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@rollback')->name('io_generator_builder_rollback');

    Route::post(
        'generator_builder/generate-from-file',
        '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generateFromFile'
    )->name('io_generator_builder_generate_from_file');
});

Route::get('new_contract', 'HomeController@new_contract')->name('new_contract');
Route::post('new_contract/save', 'HomeController@contract_save')->name('contract_save');
Route::get('contracts_list', 'HomeController@contracts_list')->name('contracts_list');

Route::get('contract/edit/{id}', 'HomeController@contract_edit')->name('contract_edit');
Route::get('contract/detail/{id}', 'HomeController@contract_detail')->name('contract_detail');
Route::get('contract/delete/{id}', 'HomeController@contract_delete')->name('contract_delete');
Route::post('contract/update/{id}', 'HomeController@contract_update')->name('contract_update');

// call
Route::post('contract/call/{option}/{contract_id}', 'HomeController@contract_call')->name('contract_call');
Route::get('call/delete/{call_id}', 'HomeController@call_delete')->name('call_delete');

// user
Route::get('new_user', 'UsersController@new_user')->name('new_user');
Route::post('new_user/save', 'UsersController@user_save')->name('user_save');
Route::get('users_list', 'UsersController@users_list')->name('users_list');

Route::get('user/edit/{id}', 'UsersController@user_edit')->name('user_edit');
Route::get('user/detail/{id}', 'UsersController@user_detail')->name('user_detail');
Route::get('user/delete/{id}', 'UsersController@user_delete')->name('user_delete');
Route::post('user/update/{id}', 'UsersController@user_update')->name('user_update');

// animation
Route::get('animations', 'HomeController@animation')->name('animation');

// department
Route::get('new_department', 'DepartmentController@new_department')->name('new_department');
Route::post('new_department/save', 'DepartmentController@department_save')->name('department_save');
Route::get('departments_list', 'DepartmentController@departments_list')->name('departments_list');
Route::get('department/edit/{id}', 'DepartmentController@department_edit')->name('department_edit');
Route::get('department/detail/{id}', 'DepartmentController@department_detail')->name('department_detail');
Route::get('department/delete/{id}', 'DepartmentController@department_delete')->name('department_delete');
Route::post('department/update/{id}', 'DepartmentController@department_update')->name('department_update');


Route::get('/home', 'HomeController@index')->name('home');
Route::post('check_contract', 'HomeController@check_contract')->name('check_contract');

Route::get('{name?}', 'JoshController@showView');