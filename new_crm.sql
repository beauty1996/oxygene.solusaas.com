/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 8.0.22 : Database - new_crm
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`new_crm` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `new_crm`;

/*Table structure for table `calls` */

DROP TABLE IF EXISTS `calls`;

CREATE TABLE `calls` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `option` int NOT NULL,
  `contract_id` int NOT NULL,
  `ext` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uploaded_by` int NOT NULL,
  `is_deleted` int DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `calls` */

insert  into `calls`(`id`,`name`,`file_path`,`option`,`contract_id`,`ext`,`type`,`uploaded_by`,`is_deleted`,`created_at`,`updated_at`) values 
(3,'Screenshot_5.jpg','/uploads/call_files/Screenshot_5.jpg',2,7,'jpg','extra',1,0,'2020-12-02 03:15:59','2020-12-02 03:15:59'),
(4,'Screenshot_2.jpg','/uploads/call_files/Screenshot_2.jpg',1,7,'jpg','extra',1,0,'2020-12-02 03:17:13','2020-12-02 03:17:13'),
(5,'Screenshot_5.jpg','/uploads/call_files/Screenshot_5.jpg',1,7,'jpg','image',1,0,'2020-12-02 03:23:33','2020-12-02 03:23:33'),
(6,'rest.mp4','/uploads/call_files/rest.mp4',1,7,'mp4','other',1,0,'2020-12-02 03:23:43','2020-12-02 03:23:43'),
(9,'imgpsh_fullsize_anim (1).jpg','/uploads/call_files/imgpsh_fullsize_anim (1).jpg',2,2,'jpg','image',17,0,'2020-12-03 08:07:08','2020-12-03 08:07:08'),
(10,'referral.PNG','/uploads/call_files/referral.PNG',2,2,'PNG','other',17,0,'2020-12-03 09:54:33','2020-12-03 09:54:33'),
(18,'test.mp3','/uploads/call_files/test.mp3',1,4,'mp3','audio',20,0,'2020-12-04 12:23:31','2020-12-04 12:23:31'),
(19,'test.mp3','/uploads/call_files/test.mp3',1,4,'mp3','audio',20,0,'2020-12-04 12:23:57','2020-12-04 12:23:57'),
(20,'test.mp3','/uploads/call_files/test.mp3',1,17,'mp3','audio',20,0,'2020-12-04 12:33:05','2020-12-04 12:33:05'),
(21,'test.mp3','/uploads/call_files/test.mp3',1,4,'mp3','audio',20,0,'2020-12-04 12:38:27','2020-12-04 12:38:27'),
(22,'test.mp3','/uploads/call_files/test.mp3',2,4,'mp3','audio',17,0,'2020-12-04 12:42:46','2020-12-04 12:42:46'),
(23,'test.mp3','/uploads/call_files/test.mp3',2,17,'mp3','audio',17,0,'2020-12-04 12:50:04','2020-12-04 12:50:04'),
(24,'test.mp3','/uploads/call_files/test.mp3',1,25,'mp3','audio',20,0,'2020-12-04 21:59:54','2020-12-04 21:59:54'),
(25,'test.mp3','/uploads/call_files/test.mp3',2,25,'mp3','audio',17,0,'2020-12-04 22:04:14','2020-12-04 22:04:14'),
(26,'test.mp3','/uploads/call_files/test.mp3',1,1,'mp3','audio',1,0,'2020-12-05 06:07:50','2020-12-05 06:07:50');

/*Table structure for table `contract_statuses` */

DROP TABLE IF EXISTS `contract_statuses`;

CREATE TABLE `contract_statuses` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `contract_id` int NOT NULL,
  `updated_by` int NOT NULL,
  `option` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `contract_statuses` */

/*Table structure for table `contracts` */

DROP TABLE IF EXISTS `contracts`;

CREATE TABLE `contracts` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `Nom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `Prénom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `naissance` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `Téléphone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `d’appel` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '1',
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `Postal` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `Vendu` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `Mensuelle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `Financier` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CALL1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CALL2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Comments` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int NOT NULL,
  `checked` int DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `contracts` */

insert  into `contracts`(`id`,`Nom`,`Prénom`,`naissance`,`Téléphone`,`d’appel`,`email`,`Postal`,`Vendu`,`Mensuelle`,`Financier`,`CALL1`,`CALL2`,`Comments`,`created_by`,`checked`,`created_at`,`updated_at`) values 
(1,'contract','last','2020-12-11','12345678','2','contract@gmail.com','123451','[\"PJ\"]','1234','7',NULL,NULL,'12345',1,1,'2020-12-05 06:07:23','2020-12-05 06:07:38');

/*Table structure for table `department_users` */

DROP TABLE IF EXISTS `department_users`;

CREATE TABLE `department_users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `department_id` int NOT NULL,
  `option` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `department_users` */

insert  into `department_users`(`id`,`user_id`,`department_id`,`option`,`created_at`,`updated_at`) values 
(1,23,1,NULL,'2020-12-05 06:08:27','2020-12-05 06:08:27'),
(2,24,1,2,'2020-12-05 06:30:39','2020-12-05 06:30:39'),
(3,24,2,2,'2020-12-05 06:30:53','2020-12-05 06:30:53');

/*Table structure for table `departments` */

DROP TABLE IF EXISTS `departments`;

CREATE TABLE `departments` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `departments` */

insert  into `departments`(`id`,`name`,`created_at`,`updated_at`) values 
(1,'Default Department',NULL,NULL),
(2,'new dep1','2020-12-05 06:30:39','2020-12-05 06:30:53');

/*Table structure for table `failed_jobs` */

DROP TABLE IF EXISTS `failed_jobs`;

CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `failed_jobs` */

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(1,'2014_10_12_000000_create_users_table',1),
(2,'2014_10_12_100000_create_password_resets_table',1),
(3,'2019_08_19_000000_create_failed_jobs_table',1),
(4,'2020_11_27_181004_create_contracts_table',2),
(5,'2020_11_27_184138_create_roles_table',3),
(6,'2020_11_27_184213_create_role_users_table',4),
(7,'2020_11_27_184254_create_statuses_table',5),
(8,'2020_11_27_211538_create_financiers_table',6),
(9,'2020_11_28_192125_create_departments_table',6),
(10,'2020_11_29_035250_create_department_users_table',7),
(11,'2020_11_30_163641_create_calls_table',8),
(12,'2020_12_02_142200_create_dapples_table',9),
(13,'2020_12_03_091846_create_contract_statuses_table',10);

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `role_users` */

DROP TABLE IF EXISTS `role_users`;

CREATE TABLE `role_users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int NOT NULL,
  `user_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `role_users` */

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `displayname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `roles` */

insert  into `roles`(`id`,`name`,`displayname`,`created_at`,`updated_at`) values 
(1,'Admin','admin',NULL,NULL),
(2,'Supervisor','supervisor',NULL,NULL),
(3,'Agent','agent',NULL,NULL),
(4,'GESTIONNAIRE','GESTIONNAIRE',NULL,NULL),
(5,'Responsable Gestion','Responsable Gestion',NULL,NULL),
(6,'Animationer','Animationer',NULL,NULL);

/*Table structure for table `statuses` */

DROP TABLE IF EXISTS `statuses`;

CREATE TABLE `statuses` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `option` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `statuses` */

insert  into `statuses`(`id`,`name`,`color`,`option`,`icon`,`created_at`,`updated_at`) values 
(1,'En attente conformité','Grey','dapple','im-icon-Checked-User',NULL,NULL),
(2,'En attente de Validation','Blue','dapple','im-icon-Love-User',NULL,NULL),
(3,'Validé','Green','dapple','im-icon-Eye-Scan',NULL,NULL),
(4,'Annulé','Red','dapple','im-icon-Add-Cart',NULL,NULL),
(5,'Injoignable','Yellow','dapple','im-icon-Aligator',NULL,NULL),
(6,'Anomalie','Black','dapple','im-icon-Airbrush',NULL,NULL),
(7,'Impayée','Orange','finance','im-icon-Administrator',NULL,NULL),
(8,'Payé','#71b37d','finance','im-icon-Add-Bag',NULL,NULL);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` int DEFAULT NULL,
  `role_id` int DEFAULT NULL,
  `department_id` int DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`address`,`telephone`,`role_id`,`department_id`,`email_verified_at`,`created_by`,`password`,`remember_token`,`created_at`,`updated_at`) values 
(1,'admin','admin@admin.com','address',13212321,1,1,NULL,1,'$2y$10$WoROK4FDZTT3WSaeuHznMeAEJxntwR8SZIgKeggMI7qQ8I1FvrRVy','tYiKZU3cphvNcWxMSOhlNcRkQhvOPLcrPfG4MSkgnS9AoOC59UB6oON6KXzD','2020-11-27 15:54:41','2020-12-03 11:12:45'),
(23,'agent','agent@gmail.com','address',1234562,3,NULL,NULL,NULL,'$2y$10$JQJdApJVWaJcbrMiPPK2POOa8XJEWcgWXXxME/wE4C.40lgD2rxv.',NULL,'2020-12-05 06:08:27','2020-12-05 06:08:27'),
(24,'super','super@gmail.com','address',985632566,2,NULL,NULL,NULL,'$2y$10$rtIr/567NrLL6tqdBHUnpuGDvTPHQDftgHYADe4YlpGy9GxqgGYga',NULL,'2020-12-05 06:09:33','2020-12-05 06:09:33');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
