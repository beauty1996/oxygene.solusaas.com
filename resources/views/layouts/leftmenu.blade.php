<div id="menu" role="navigation">
    <header class="header navbar navbar-expand-sm">
        <ul class="navbar-nav theme-brand flex-row  text-center">
        <a href="{{ URL::to('') }}" class="mr-0">
            <li class="nav-item theme-text">
                <img class="logo-img" src="{{asset('img/logo-mma_11466.jpg')}}">
            </li>
        </a>
        </ul>
    </header>
    <ul class="navigation list-unstyled" id="demo">
        <li><span class="close-icon d-xl-none d-lg-block"><img src="{{asset('img/images/input-disabled.png')}}"
                    alt="image missing"></span></li>

        <a href="{{ URL::to('') }}" class="mr-0">
            <h1 class="text-center">{{auth()->user()->name}}</h1>
        </a>
        <li {!! (Request::is('') ? 'class="active"' : '' ) !!}>
            <a href="{{ URL::to('') }}">
                <!-- <span class="livicon activeicon" data-name="check-circle-alt" data-color="blue"></span> -->
                <span class="mm-text">
                    <span class="livicon" data-name="home">Dashboard</span>
                </span>
                
                <span class="mm-text">
                    <div class="icon text-center">
                        <label>
                            
                        </label>
                    </div>
                </span>
            </a>
        </li>

        @if(auth()->user()->role_id != 6 )
        <li {!! (Request::is('new_contract') || Request::is('contracts_list') ? 'class="menu-dropdown active"'
            : "class='menu-dropdown'" ) !!}>
            <a href="#">
                <span class="mm-text ">
                    <span class="livicon" data-name="timer"></span>
                    Contracts
                </span>
                <span class="menu-icon">
                    <div class="icon text-center">
                        <label>
                            <!-- <div class="livicon" data-name="timer"></div> -->
                        </label>
                    </div>
                </span>
                <span class="im im-icon-Arrow-Right imicon"></span>
            </a>
            <ul class="sub-menu list-unstyled">
                <li {!! (Request::is('new_contract') ? 'class="active"' : '' ) !!}>
                    <a href="{{ URL::to('new_contract') }}">
                        <span class="mm-text ">Add New Contract</span>
                    </a>
                </li>
                @if(auth()->user()->role_id != 3)
                <li {!! (Request::is('contracts_list') ? 'class="active"' : '' ) !!}>
                    <a href="{{ URL::to('contracts_list') }}">
                        Contracts List
                    </a>
                </li>
                @endif
            </ul>
        </li>
        @endif

        @if(auth()->user()->role_id == 1)
        <li {!! (Request::is('new_user') || Request::is('users_list') ? 'class="menu-dropdown active"'
            : "class='menu-dropdown'" ) !!}>
            <a href="#">
                <span class="mm-text ">
                    <span class="livicon" data-name="users"></span>
                    Users
                </span>
                <span class="menu-icon ">
                    <div class="icon text-center">
                        <label>
                            
                        </label>
                    </div>
                </span>
                <span class="im im-icon-Arrow-Right imicon"></span>
            </a>
            <ul class="sub-menu list-unstyled">
                <li {!! (Request::is('new_user') ? 'class="active"' : '' ) !!}>
                    <a href="{{ URL::to('new_user') }}">
                        <span class="mm-text ">New User</span>
                    </a>
                </li>
                <li {!! (Request::is('users_list') ? 'class="active"' : '' ) !!}>
                    <a href="{{ URL::to('users_list') }}">
                        Users List
                    </a>
                </li>
            </ul>
        </li>
        
        <!-- start -->
        <li {!! (Request::is('new_department') || Request::is('departments_list') ? 'class="menu-dropdown active"'
            : "class='menu-dropdown'" ) !!}>
            <a href="#">
                <span class="mm-text ">
                    <span class="livicon" data-name="inbox"></span>
                    Dep
                </span>
                <span class="menu-icon ">
                    <div class="icon text-center">
                        <label>
                            
                        </label>
                    </div>
                </span>
                <span class="im im-icon-Arrow-Right imicon"></span>
            </a>
            <ul class="sub-menu list-unstyled">
                <li {!! (Request::is('new_department') ? 'class="active"' : '' ) !!}>
                    <a href="{{ URL::to('new_department') }}">
                        <span class="mm-text ">New Dep</span>
                    </a>
                </li>
                <li {!! (Request::is('departments_list') ? 'class="active"' : '' ) !!}>
                    <a href="{{ URL::to('departments_list') }}">
                        Departments List
                    </a>
                </li>
            </ul>
        </li>
        <!-- end -->
        @endif
        
        @if(in_array(auth()->user()->role_id, [1, 6]))
        <li {!! (Request::is('animation') ? 'class="active"' : '' ) !!}>
            <a href="{{ URL::to('animations') }}">
                <span class="mm-text ">
                    <span class="livicon" data-name="spinner-seven"></span>
                    Animation
                </span>
                <span class="menu-icon">
                    <div class="icon text-center">
                        <label>
                        </label>
                    </div>
                </span>
            </a>
        </li>
        @endif
        
        @include("layouts/menu")
    </ul>
    <!-- / .navigation -->
</div>
