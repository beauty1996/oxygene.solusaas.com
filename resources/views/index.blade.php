@extends('layouts.default')
{{-- Page title --}}
@section('title')
Dashboard @parent
@stop
{{-- page level styles --}}
@section('header_styles')
<!-- page vendors -->
<link href="{{ asset('css/pages.css')}}" rel="stylesheet">


<!--end of page vendors -->
@stop
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <div aria-label="breadcrumb" class="card-breadcrumb">
        <h1>Dashboard</h1>

    </div>
    <div class="separator-breadcrumb border-top"></div>
</section>
<!-- /.content -->
<section class="content">
    <div class="row">
        @if(count($data['total']) > 0)
        @foreach($data['total'] as $temp)
        @if($temp['option'] == 'dapple')
        <div class="col-md-6 col-xl-3 col-12 mb-20">
            <div class="bg-white dashboard-col pl-15 pb-15 pt-15">
                <i class="im {{ $temp['icon'] }} im-icon-set float-right" style="background:{{ $temp['color'] }};"></i>
                <h3>{{ $temp['count'] }}</h3>
                <p>{{ $temp['name'] }}</p>
            </div>
        </div>
        @endif
        @endforeach
        @endif
    </div>

    <div class="row">
        <div class="col-md-4">
            <div id="dappel_chart"></div>
        </div>
        @if(in_array(auth()->user()->role_id, [1, 5]))
        <div class="col-md-4">
            <div id="financial_chart"></div>
        </div>
        <div class="col-md-4">
            <div id="product_chart"></div>
        </div>
        @endif
    </div>

</section>

@stop
@section('footer_scripts')
<!--   page level js ----------->
<script language="javascript" type="text/javascript" src="{{ asset('vendors/chartjs/js/Chart.js') }}"></script>
<script src="{{ asset('js/pages/dashboard.js') }}"></script>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
    // Load google charts
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    // Draw the chart and set the chart values
    function drawChart() {
    var data = google.visualization.arrayToDataTable(
        JSON.parse(JSON.stringify(<?php echo json_encode($data['dapple']); ?>))
    );

    // Optional; add a title and set the width and height of the chart
    var options = {'title':'Status d’appels field ', 'width':550, 'height':400};
    // Display the chart inside the <div> element with id="piechart"
    var chart = new google.visualization.PieChart(document.getElementById('dappel_chart'));
    chart.draw(data, options);

@if(in_array(auth()->user()->role_id, [1, 5]))
    var financial_data = google.visualization.arrayToDataTable(
        JSON.parse(JSON.stringify(<?php echo json_encode($data['financial']); ?>))
    );
    var financial_chart = new google.visualization.PieChart(document.getElementById('financial_chart'));
    var options = {'title':'Status Financier field ', 'width':550, 'height':400};
    financial_chart.draw(financial_data, options);
    
    var product_data = google.visualization.arrayToDataTable(
        JSON.parse(JSON.stringify(<?php echo json_encode($data['product']); ?>))
    );
    var product_chart = new google.visualization.PieChart(document.getElementById('product_chart'));
    var options = {'title':'Produit Vendu field ', 'width':550, 'height':400};
    product_chart.draw(product_data, options);
@endif

}
</script>
<!-- end of page level js -->
@stop
