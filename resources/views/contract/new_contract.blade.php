@extends('layouts.default')
{{-- Page title --}}
@section('title')
Cards @parent
@stop
{{-- page level styles --}}
@section('header_styles')
<!-- page vendors -->
<link href="{{ asset('css/pages.css')}}" rel="stylesheet">
<!-- select vendors -->
<link href="{{ asset('vendors/select2/css/select2.min.css') }}" rel="stylesheet" />
<link href="{{ asset('vendors/select2/css/select2-bootstrap4.min.css')}}" rel="stylesheet">
<!--end of page vendors -->
@stop
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">

    <div aria-label="breadcrumb" class="card-breadcrumb">
        <h1>New Contract</h1>
        <ol class="breadcrumb">
            @if(auth()->user()->role_id != 3)
            <li class="breadcrumb-item"><a href="{{ route('users_list') }}">Contracts</a></li>
            @endif
            <li class="breadcrumb-item active" aria-current="page">New</li>
        </ol>
    </div>
    <div class="separator-breadcrumb border-top"></div>
</section>
<!-- /.content -->
<section class="content">
    <div class="card p-0">
        <form class="form-horizontal" method="POST" action="{{route('contract_save')}}" enctype="multipart/form-data">
        {{ csrf_field() }}
            <div class="row mt-3 mb-3 ml-3 mr-3">
                <div class="col-3 col-md-3 col-lg-3">
                    <!-- <div class="form-group">
                        <label>Nom du Créateur</label>
                        <input type="text" name="Créateur" class="form-control" required />
                    </div>
                    <div class="form-group">
                        <label>Date et heure de saisie</label>
                        <input type="date" name="saisie" class="form-control" />
                    </div> -->
                    <div class="form-group">
                        <label>Nom</label>
                        <input type="text" name="Nom" class="form-control" required />
                    </div>
                    <div class="form-group">
                        <label>Prénom</label>
                        <input type="text" name="Prénom" class="form-control" required />
                    </div>
                    <div class="form-group">
                        <label>Date de naissance</label>
                        <input type="date" name="naissance" class="form-control" required />
                    </div>
                </div>

                <div class="col-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label>N° de Téléphone</label>
                        <input type="text" name="Téléphone" class="form-control" required />
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" name="email" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label>Adresse Postal</label>
                        <input type="text" name="Postal" class="form-control" required />
                    </div>
                </div>

                <div class="col-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label>Produit Vendu</label>
                        <select name="Vendu[]" multiple placeholder="Select Known Language" data-allow-clear="1" required>
                            <option value="IJH">IJH</option>
                            <option value="GAV">GAV</option>
                            <option value="PJ">PJ</option>
                            <option value="DCA">DCA</option>
                            <option value="ASSISTANCE">ASSISTANCE</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Cotisation Mensuelle</label>
                        <input type="text" name="Mensuelle" class="form-control" required />
                    </div>
                    @if(in_array(auth()->user()->role_id, [1, 4, 5]))
                    <div class="form-group">
                        <label>Status d’appel</label>
                        <select name="d’appel" class="form-control" placeholder="Select d’appel" data-allow-clear="1" required>
                            <option></option>
                            @if(count($statuses) > 0)
                            @foreach($statuses as $temp)
                                @if($temp->option == 'dapple')
                                <option value="{{$temp->id}}">{{$temp->name}}</option>
                                @endif
                            @endforeach
                            @endif
                        </select>
                    </div>
                    @endif
                </div>
                
                <div class="col-3 col-md-3 col-lg-3">
                    @if(in_array(auth()->user()->role_id, [1, 5]))
                    <div class="form-group">
                        <label>Status Financier</label>
                        <select class="form-control" name="Financier" placeholder="Select Financier" data-allow-clear="1" required>
                            <option></option>
                            @if(count($statuses) > 0)
                            @foreach($statuses as $temp)
                                @if($temp->option == 'finance')
                                <option value="{{$temp->id}}">{{$temp->name}}</option>
                                @endif
                            @endforeach
                            @endif
                        </select>
                    </div>
                    @endif
                    <div class="form-group">
                        <label>Comments</label>
                        <textarea type="text" cols="" rows="5" name="Comments" class="form-control" required></textarea>
                    </div>
                    
                </div>

            </div>
            <div class="row mt-3 mb-3 ml-3 mr-3">
                <button type="submit" class="btn btn-success"> Save </button>
                <button type="reset" class="btn btn-warning hidden-xs" value="Reset">Reset</button>
            </div>
        </form>
    </div>
</section>

@stop
@section('footer_scripts')
<!--   page level js ----------->
<script src="{{ asset('vendors/select2/js/select2.min.js') }}"></script>
<script type="text/javascript">
    $(function () { 
        $('select').each(function () {
            $(this).select2({
                theme: 'bootstrap4',
                width: 'style',
                placeholder: $(this).attr('placeholder'),
                allowClear: Boolean($(this).data('allow-clear')),
            });
        });
    });
</script>

<script>
// $(document).ready(function() {
//     $(".js-example-templating").select2({
//         templateResult: formatState
//     });
// });
</script>
<!-- end of page level js -->
@stop
