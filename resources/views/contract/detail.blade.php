@extends('layouts.default')
{{-- Page title --}}
@section('title')
Cards @parent
@stop
{{-- page level styles --}}
@section('header_styles')
<!-- page vendors -->
<link href="{{ asset('css/pages.css')}}" rel="stylesheet">
<!-- select vendors -->
<link href="{{ asset('vendors/select2/css/select2.min.css') }}" rel="stylesheet" />
<link href="{{ asset('vendors/select2/css/select2-bootstrap4.min.css')}}" rel="stylesheet">
<!--end of page vendors -->
@stop
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">

    <div aria-label="breadcrumb" class="card-breadcrumb">
        <h1>Contract Detail</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('contracts_list')}}">Contracts</a></li>
            <li class="breadcrumb-item active" aria-current="page"></li>
        </ol>
    </div>
    <div class="separator-breadcrumb border-top"></div>
</section>
<!-- /.content -->
<section class="content">
    <div class="card p-0">
        <form class="form-horizontal" method="POST" action="{{route('contract_save')}}" enctype="multipart/form-data">
            <div class="row mt-3 mb-3 ml-3 mr-3">
                <div class="col-3 col-md-3 col-lg-3">
                    <!-- <div class="form-group">
                        <label>Nom du Créateur</label>
                        <p>{{ $contract->Créateur}}</p>
                    </div>
                    <div class="form-group">
                        <label>Date et heure de saisie</label>
                        <p>{{$contract->saisie}}</p>
                    </div> -->
                    <div class="form-group">
                        <label>Nom</label>
                        <p>{{$contract->Nom}}</p>
                    </div>
                    <div class="form-group">
                        <label>Prénom</label>
                        <p>{{$contract->Prénom}}</p>
                    </div>
                    <div class="form-group">
                        <label>Date de naissance</label>
                        <p>{{$contract->naissance}}</p>
                    </div>
                </div>

                <div class="col-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label>N° de Téléphone</label>
                        <p>{{$contract->Téléphone}}</p>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <p>{{$contract->email}}</p>
                    </div>
                    <div class="form-group">
                        <label>Adresse Postal</label>
                        <p>{{$contract->Postal}}</p>
                    </div>
                </div>

                <div class="col-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label>Produit Vendu</label>
                        <?php 
                            $contracts = json_decode($contract->Vendu, true);
                            foreach($contracts as $temp) {
                                echo "<p> - ".$temp."</p>";
                            }
                        ?>
                    </div>
                    <div class="form-group">
                        <label>Cotisation Mensuelle</label>
                        <p>{{$contract->Mensuelle}}</p>
                    </div>
                </div>

                <div class="col-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label>Status d’appel</label>
                        <p>{{ $contract->dStatus->name }}</p>
                    </div>
                    @if(auth()->user()->role_id == 1 || auth()->user()->role_id == 1)
                    <div class="form-group">
                        <label>Status Financier</label>
                        <p>{{$contract->fStatus->name}}</p>
                    </div>
                    @endif
                    <!-- <div class="form-group">
                        <label>CALL 1</label>
                        <input type="text" name="CALL1" class="form-control" required />
                    </div>
                    <div class="form-group">
                        <label>CALL 2</label>
                        <input type="text" name="CALL2" class="form-control" required />
                    </div> -->
                    <div class="form-group">
                        <label>Comments</label>
                        <p>{{ $contract->Comments }}</p>
                    </div>
                </div>

            </div>
           
        </form>
    </div>
</section>
<section class="content">
    <div class="card p-0">
        <div class="mt-3 mr-3 ml-3 mb-3">
            <div class="form-group">
                <label>Historique d’actions</label>
                <p> - {{ $contract->created_at.' '.$contract->user->name }} has added the contract</p>
                <!-- call1 files -->
                @if(count($call_files) > 0)
                @foreach($call_files as $temp) 
                    @if($temp->option == 1)
                        @if($temp->is_deleted != 1)
                        <p> - {{ $temp->created_at.' '.$temp->upload_user->name}} à ajouter l’enregistrement du call1</p>
                        @else
                        <p> - {{ $temp->created_at.' '.$temp->upload_user->name}} à ajouter deleted du call1</p>
                        @endif
                    @else    
                        @if($temp->is_deleted != 1)
                        <p> - {{ $temp->created_at.' '.$temp->upload_user->name}} à ajouter l’enregistrement du call2</p>
                        @else
                        <p> - {{ $temp->created_at.' '.$temp->upload_user->name}} à ajouter deleted du call2</p>
                        @endif
                    @endif
                @endforeach
                @endif
                <!-- changed status -->
                @if(count($change_status) > 0)
                @foreach($change_status as $temp) 
                    @if($temp->option == 1)
                    <p> - {{ $temp->created_at.' '.$temp->uploader->name}} à changer le status d’appel du contrat</p>
                    @else
                    <p> - {{ $temp->created_at.' '.$temp->uploader->name}} à changer le status financier du contrat</p>
                    @endif
                @endforeach
                @endif
            </div>
        </div>
    </div>
</section>
@stop
@section('footer_scripts')
<!--   page level js ----------->

<script>
// $(document).ready(function() {
//     $(".js-example-templating").select2({
//         templateResult: formatState
//     });
// });
</script>
<!-- end of page level js -->
@stop
