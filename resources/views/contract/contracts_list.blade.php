@extends('layouts.default')
{{-- Page title --}}
@section('title')
Advanced Data Tables @parent
@stop
{{-- page level styles --}}
@section('header_styles')
<!-- page vendors -->
<!-- <link rel="stylesheet" type="text/css" href="{{ asset('vendors/datatables/css/dataTables.bootstrap4.min.css') }}" />
<link rel="stylesheet" href="{{asset('vendors/datatables/css/buttons.bootstrap4.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/datatables/css/rowReorder.bootstrap4.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/datatables/css/scroller.bootstrap4.css') }}"> -->
{{--    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/select2/css/select2.min.css') }}" />--}}
{{--    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/select2/css/select2-bootstrap.css') }}" />--}}
<!--end of page vendors -->
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css"> -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.bootstrap4.min.css">
<style>
thead input {
        width: 100%;
    }
</style>
@stop
@section('content')

<!-- content start-->
<section class="content">
    <div class="row">
        <div class="col-lg-12 my-3">
            <div class="card panel-info filterable">

            <div id="reportrange" class="btn btn-success btn-lg">
                <span></span> <b class="caret"></b>
            </div>
            <hr>
                <div class="card-body table-responsive table-responsive-lg table-responsive-md table-responsive-sm">
                    <table class="table table-bordered table-striped contract_list_table" id="table1" width="100%">
                        <thead>
                            <tr>
                                <th>Id</id>
                                <!-- <th>Créateur</th> -->
                                <!-- <th>saisie</th> -->
                                <th>Nom</th>
                                <th>Prénom</th>
                                <!-- <th>naissance</th> -->
                                <th>Téléphone</th>
                                <th>email</th>
                                <th>Comments</th>
                                <th>Status</th>
                                <th>Created By</th>
                                <th>Created</th>
                                <th>
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        @if(count($contracts_list) > 0)
                            @foreach($contracts_list as $temp)
                            <tr style="background: {{ $temp->dStatus->color }}">
                                <th>{{$temp->id}}</id>
                                <td>{{$temp->Nom}}</td>
                                <td>{{$temp->Prénom}}</td>
                                <td>{{$temp->Téléphone}}</td>
                                <td>{{$temp->email}}</td>
                                <td>{{$temp->Comments}}</td>
                                <td>{{$temp->dStatus->name}}</td>
                                <td>{{$temp->user->name}}</td>
                                <td>{{$temp->created_at}}</td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-dark btn-sm dropdown-toggle dropdown-toggle-split" id="dropdownMenuReference1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-reference="parent">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuReference1">
                                            <a class="dropdown-item" href="{{route('contract_detail', $temp->id)}}">Détails</a>
                                            <a class="dropdown-item" href="{{route('contract_edit', $temp->id)}}">Modifier</a>
                                            @if($temp->canDelete($temp->id))
                                            <a class="dropdown-item" onClick="if(confirm('Are you sure?')) window.location.href= '{{ route("contract_delete", $temp->id) }}'">Supprimer</a>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</section>
<!-- content end-->

@stop
@section('footer_scripts')
<!--   page level js ----------->
<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.bootstrap4.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/fixedheader/3.1.7/js/dataTables.fixedHeader.min.js"></script>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js" integrity="sha512-LGXaggshOkD/at6PFNcp2V2unf9LzFq6LE+sChH7ceMTDP0g2kn6Vxwgg7wkPP7AAtX+lmPqPdxB47A0Nz0cMQ==" crossorigin="anonymous"></script> -->
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />


<script>
$(document).ready(function() {
    var table = $('#table1').DataTable( {
        // lengthChange: false,        
        dom: 'Blfrtip',
		responsive: true,
        buttons: [ 
            {
                extend: 'csv',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                }
            },
            {
                extend: 'excel',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                }
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 5 ]
                }
            },
			
            'colvis'
		
        ],
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "tous"]],
		language: {
			'lengthMenu':'Affichage de _MENU_ Contrats par page',
			"sEmptyTable":      "Aucune donnée disponible dans le tableau",
		    "sInfo":            "_START_ à _END_ de _TOTAL_ Contrats",
		    "sInfoEmpty":       "0 à 0 de 0 Contrats",
		   "sInfoFiltered":    "(filtré de _MAX_ Contrats)",
		   "sInfoPostFix":     "",
		   "sInfoThousands":   ".",
		   "sLengthMenu":      "_MENU_ afficher les Contrats",
		   "sLoadingRecords":  "Chargement...",
		   "sProcessing":      "S'il vous plaît, attendez...",
		   "sSearch":          "Recherche",
		   "sZeroRecords":     "Aucune Contrat disponible.",
		   "oPaginate": {
			   "sFirst":       "Première",
			   "sPrevious":    "Précédente",
			   "sNext":        "Suivante",
			   "sLast":        "Dernière"
		   },
			"buttons" : {
				"csv":"CSV",
				"excel":"EXCEL",
				"pdf":"PDF",
				"colvis": "Affichage des colonnes"
			},
		   "oAria": {
			"sSortAscending":  ": Activer pour trier les colonnes par ordre croissant",
			"sSortDescending": ": Activer pour trier la colonne par ordre décroissant"
		   }
		}
    } );
 
    table.buttons().container()
        .appendTo( '#table1_wrapper .col-md-6:eq(0)' );

        // filter script
    $('#table1 thead tr').clone(true).appendTo( '#table1 thead' );
    $('#table1 thead tr:eq(1) th').each( function (i) {
        if(i == 9)
            return false;
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
 
        $( 'input', this ).on( 'keyup change', function () {
            if ( table.column(i).search() !== this.value ) {
                table
                    .column(i)
                    .search( this.value )
                    .draw();
            }
        } );
    } );
    
    // date range
        
    $(function() {
        var start = moment().subtract(29, 'days');
        var end = moment();

        function cb(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "MM/DD/YYYY",
                "separator": " - ",
                "applyLabel": "Apply",
                "cancelLabel": "Cancel",
                "fromLabel": "From",
                "toLabel": "To",
                "customRangeLabel": "Custom",
                "daysOfWeek": [
                    "Su",
                    "Mo",
                    "Tu",
                    "We",
                    "Th",
                    "Fr",
                    "Sat"
                ],
                "monthNames": [
                    "January",
                    "February",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "Decembersss"
                ],
                "firstDay": 1
            }
        }, cb);
        
        cb(start, end);
    });
    
    $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
        var start = picker.startDate;
        var end = picker.endDate;

        $.fn.dataTable.ext.search.push(
            function(settings, data, dataIndex) {
                var min = start;
                var max = end;
                var startDate = new Date(data[8]);  //match with table field
                
                if (min == null && max == null) {
                    return true;
                }
                if (min == null && startDate <= max) {
                    return true;
                }
                if (max == null && startDate >= min) {
                    return true;
                }
                if (startDate <= max && startDate >= min) {
                    return true;
                }

                return false;
            }
        );
        table.draw();
        $.fn.dataTable.ext.search.pop();
    });
} );
</script>
@stop
