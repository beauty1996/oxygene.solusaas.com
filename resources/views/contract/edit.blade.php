@extends('layouts.default')
{{-- Page title --}}
@section('title')
Cards @parent
@stop
{{-- page level styles --}}
@section('header_styles')
<!-- page vendors -->
<link href="{{ asset('css/pages.css')}}" rel="stylesheet">
<!-- select vendors -->
<link href="{{ asset('vendors/select2/css/select2.min.css') }}" rel="stylesheet" />
<link href="{{ asset('vendors/select2/css/select2-bootstrap4.min.css')}}" rel="stylesheet">
<!--end of page vendors -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/dropzone.min.css" />
<style>
    .attached_files{
        width:250px;
    }
</style>    
@stop
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">

    <div aria-label="breadcrumb" class="card-breadcrumb">
        <h1>Edit Contract</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('contracts_list')}}">Contracts</a></li>
            <li class="breadcrumb-item active" aria-current="page">Edit</li>
        </ol>
    </div>
    <div class="separator-breadcrumb border-top"></div>
</section>
<!-- /.content -->
<section class="content">
    <div class="card p-0">
        <form class="form-horizontal" method="POST" action="{{route('contract_update', ['id' => $contract->id ])}}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" value="{{ $contract->id }}" name="contract_id" />
            <div class="row mt-3 mb-3 ml-3 mr-3">
                <div class="col-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label>Nom</label>
                        <input type="text" value="{{ $contract->Nom }}" name="Nom" class="form-control" required />
                    </div>
                    <div class="form-group">
                        <label>Prénom</label>
                        <input type="text" value="{{ $contract->Prénom }}" name="Prénom" class="form-control" required />
                    </div>
                    <div class="form-group">
                        <label>Date de naissance</label>
                        <input type="date" value="{{ $contract->naissance }}" name="naissance" class="form-control" required />
                    </div>
                </div>

                <div class="col-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label>N° de Téléphone</label>
                        <input type="text" value="{{ $contract->Téléphone }}" name="Téléphone" class="form-control" required />
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" name="email" value="{{ $contract->email }}" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label>Adresse Postal</label>
                        <input type="text" value="{{ $contract->Postal }}" name="Postal" class="form-control" required />
                    </div>
                </div>

                <div class="col-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label>Produit Vendu</label>
                        <select name="Vendu[]" multiple placeholder="Select Known Language" data-allow-clear="1" required>
                            <option value="IJH" {{ in_array("IJH", json_decode($contract->Vendu, true)) ? "selected" : "" }}>IJH</option>
                            <option value="GAV" {{ in_array("GAV", json_decode($contract->Vendu, true)) ? "selected" : "" }}>GAV</option>
                            <option value="PJ" {{ in_array("PJ", json_decode($contract->Vendu, true)) ? "selected" : "" }}>PJ</option>
                            <option value="DCA" {{ in_array("DCA", json_decode($contract->Vendu, true)) ? "selected" : ""}}>DCA</option>
                            <option value="ASSISTANCE" {{ in_array("ASSISTANCE", json_decode($contract->Vendu, true)) ? "selected" : "" }}>ASSISTANCE</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Cotisation Mensuelle</label>
                        <input type="text" value="{{ $contract->Mensuelle }}" name="Mensuelle" class="form-control" required />
                    </div>
                </div>

                <div class="col-3 col-md-3 col-lg-3">
                    @if(in_array(auth()->user()->role_id, [1, 4, 5]))
                    <div class="form-group">
                        <label>Status d’appel</label>
                        <select name="d’appel" placeholder="Select Known Language" data-allow-clear="1" required>
                            <option></option>
                            @if(count($statuses) > 0)
                            @foreach($statuses as $temp)
                                @if($temp->option == 'dapple')
                                <option value="{{$temp->id}}" {{ (!empty($contract->d’appel) && $contract->d’appel == $temp->id) ? "selected" : "" }}>{{$temp->name}}</option>
                                @endif
                            @endforeach
                            @endif
                        </select>
                    </div>
                    @endif
                    @if(in_array(auth()->user()->role_id, [1, 5]))
                    <div class="form-group">
                        <label>Status Financier</label>
                        <select class="form-control" name="Financier" placeholder="Select Financier" data-allow-clear="1">
                            <option></option>
                            @if(count($statuses) > 0)
                            @foreach($statuses as $temp)
                                @if($temp->option == 'finance')
                                <option value="{{$temp->id}}" {{ (!empty($contract->Financier) && $contract->Financier == $temp->id) ? "selected" : "" }}>{{$temp->name}}</option>
                                @endif
                            @endforeach
                            @endif
                        </select>
                    </div>
                    @endif
                    <div class="form-group">
                        <label>Comments</label>
                        <textarea type="text" name="Comments" class="form-control" required>{{ $contract->Comments }}</textarea>
                    </div>
                </div>
            <div class="row mt-3 mb-3 ml-3 mr-3">
                <button type="submit" class="btn btn-success"> Update </button>
                <!-- <button type="reset" class="btn btn-warning hidden-xs" value="Reset">Reset</button> -->
            </div>
        </form>
    </div>
</section>

<section class="content">
    <div class="card p-0">
        <div class="mt-3 mr-3 ml-3 mb-3">
            <div class="form-group">
                <label>Historique d’actions</label>
                <p> - {{ $contract->created_at.' '.$contract->user->name }} has added the contract</p>
                <!-- call1 files -->
                @if(count($call_files) > 0)
                @foreach($call_files as $temp) 
                    @if($temp->option == 1)
                        @if($temp->is_deleted != 1)
                        <p> - {{ $temp->created_at.' '.$temp->upload_user->name}} à ajouter l’enregistrement du call1</p>
                        @else
                        <p> - {{ $temp->created_at.' '.$temp->upload_user->name}} à ajouter deleted du call1</p>
                        @endif
                    @else    
                        @if($temp->is_deleted != 1)
                        <p> - {{ $temp->created_at.' '.$temp->upload_user->name}} à ajouter l’enregistrement du call2</p>
                        @else
                        <p> - {{ $temp->created_at.' '.$temp->upload_user->name}} à ajouter deleted du call2</p>
                        @endif
                    @endif
                @endforeach
                @endif
                <!-- changed status -->
                @if(count($change_status) > 0)
                @foreach($change_status as $temp) 
                    @if($temp->option == 1)
                    <p> - {{ $temp->created_at.' '.$temp->uploader->name}} à changer le status d’appel du contrat</p>
                    @else
                    <p> - {{ $temp->created_at.' '.$temp->uploader->name}} à changer le status financier du contrat</p>
                    @endif
                @endforeach
                @endif
            </div>
        </div>
    </div>
</section>

<section class="content">
    <div class="card p-0">
        <div class="mt-3 mr-3 ml-3 mb-3">
            <label>Call 1</label>
            @if(in_array(auth()->user()->role_id, [1, 2]))
            <form action='{{ route("contract_call", ["option"=>1, "contract_id"=>$contract->id]) }}' method="POST" enctype="multipart/form-data" class="dropzone" id='image-upload'>
                @csrf
            </form>
            @endif
            @if(count($call_files) > 0)
                <br />
                @foreach($call_files as $temp) 
                    @if($temp->option == 1)
                    <figure>
                        <figcaption><b>{{$temp->name}}: </b></figcaption>
                        @if($temp->type == "audio")
                        <audio
                            controls
                            src="{{asset($temp->file_path)}}">
                                Your browser does not support the
                                <code>audio</code> element.
                        </audio>
                        @elseif($temp->type == "image")
                        <img class="attached_files" src="{{asset($temp->file_path)}}" />
                        @elseif($temp->ext == "mp4")
                        <video width="320" height="240" controls>
                            <source src="{{asset($temp->file_path)}}" type="video/mp4">
                            Your browser does not support the video tag.
                        </video>
                        @else
                        <a target=”_blank” href="{{asset($temp->file_path)}}">View</a>
                        @endif
                        @if(auth()->user()->role_id == 1)
                        <button class="btn btn-warning" onClick="if(confirm('Are you sure?')) window.location.href= '{{ route("call_delete", $temp->id) }}'">Delete</button>
                        @endif
                    </figure>
                    @endif
                @endforeach
            @else
            <br>
            <p>No files yet!</p>
            @endif
        </div>
    </div>
</section>    

@if(in_array(auth()->user()->role_id, [1, 4, 5]))
<section class="content">
    <div class="card p-0">
        <div class="mt-3 mr-3 ml-3 mb-3">
            <label>Call 2</label>
            <form action='{{ route("contract_call", ["option"=>2, "contract_id"=>$contract->id]) }}' method="POST" enctype="multipart/form-data" class="dropzone" id='image-upload'>
                @csrf
            </form>
            @if(count($call_files) > 0)
                <br />
                @foreach($call_files as $temp) 
                    @if($temp->option == 2)
                    <figure>
                        <figcaption><b>{{$temp->name}}: </b></figcaption>
                        @if($temp->type == "audio")
                        <audio class="attached_files"
                            controls
                            src="{{asset($temp->file_path)}}">
                                Your browser does not support the
                                <code>audio</code> element.
                        </audio>
                        @elseif($temp->type == "image")
                        <img class="attached_files" src="{{asset($temp->file_path)}}" />
                        @elseif($temp->ext == "mp4")
                        <video width="320" height="240" controls>
                            <source src="{{asset($temp->file_path)}}" type="video/mp4">
                            Your browser does not support the video tag.
                        </video>
                        @else
                        <a target=”_blank” href="{{asset($temp->file_path)}}">View</a>
                        @endif

                        @if(auth()->user()->role_id == 1)
                        <button class="btn btn-warning" onClick="if(confirm('Are you sure?')) window.location.href= '{{ route("call_delete", $temp->id) }}'">Delete</button>
                        @endif
                    </figure>
                    @endif
                @endforeach
            @else
            <br>
            <p>No files yet!</p>
            @endif
        </div>
    </div>
</section>
@endif

@stop

@section('footer_scripts')
<!--   page level js ----------->
<script src="{{ asset('vendors/select2/js/select2.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/dropzone.min.js"></script>
<script>
    $(function () {
        $('select').each(function () {
            $(this).select2({
                theme: 'bootstrap4',
                width: 'style',
                placeholder: $(this).attr('placeholder'),
                allowClear: Boolean($(this).data('allow-clear')),
            });
        });
    });
</script>

@stop