@extends('layouts.default')
{{-- Page title --}}
@section('title')
Cards @parent
@stop
{{-- page level styles --}}
@section('header_styles')
<!-- page vendors -->
<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'>
<link href="{{ asset('css/pages.css')}}" rel="stylesheet">
<!-- select vendors -->
<link href="{{ asset('vendors/select2/css/select2.min.css') }}" rel="stylesheet" />
<link href="{{ asset('vendors/select2/css/select2-bootstrap4.min.css')}}" rel="stylesheet">
<!--end of page vendors -->
<style>
/* Style the video: 100% width and height to cover the entire window */
  #myVideo {
    position: fixed;
    right: 0;
    bottom: 0;
    min-width: 100%;
    min-height: 100%;
  }

  /* Add some content at the bottom of the video/page */
  .content {
    position: fixed;
    bottom: 0;
    background: rgba(0, 0, 0, 0.5);
    color: #f1f1f1;
    width: 100%;
    padding: 20px;
  }

  /* Style the button used to pause/play the video */
  #myBtn {
    width: 200px;
    font-size: 18px;
    padding: 10px;
    border: none;
    background: #000;
    color: #fff;
    cursor: pointer;
  }

  #myBtn:hover {
    background: #ddd;
    color: black;
  }
  /* .toast-top-right {
    position: fixed;
    top: 50%;
    left: 60%;
    width: 250px;
    transform: translate(-50%, -50%);
    padding: 20px 50px 20px 20px;
  } */
  @font-face {
  font-family: 'Sigmar One';
  font-style: normal;
  font-weight: 400;
  src: url(https://fonts.gstatic.com/s/sigmarone/v11/co3DmWZ8kjZuErj9Ta3do6Tpow.ttf) format('truetype');
}
body {
  background: #3da1d1;
  overflow: hidden;
}
.congrats {
  position: absolute;
  top: 30vh;
  width: 550px;
  height: 100px;
  padding: 20px 10px;
  text-align: center;
  margin: 0 auto;
  left: 0;
  right: 0;
}
.congrats h1 {
  color: #fff;
  transform-origin: 50% 50%;
  font-size: 50px;
  font-family: 'Sigmar One', cursive;
  cursor: pointer;
  z-index: 2;
  position: absolute;
  top: 0;
  text-align: center;
  width: 100%;
}
.blob {
  height: 50px;
  width: 50px;
  color: #ffcc00;
  position: absolute;
  top: 45%;
  left: 45%;
  z-index: 1;
  font-size: 30px;
  display: none;
}
</style>    
@stop
@section('content')
<section class="contents">
    <div class="card p-0">
        <div class="congrats">
          <!-- <h1 id="cong_text">Félicitation ! [creator name ] à créer un contrat</h1> -->
        </div>
       <!-- The video -->
        <video autoplay muted id="myVideo">
            <source src="{{asset('video/video.mp4')}}" type="video/mp4">
        </video>

        <!-- Optional: some overlay text to describe the video -->
        <div class="">
          <button id="myBtn" class="hidden d-none" onclick="myFunction()">video</button>
        </div>
    </div>
</section>
@stop
@section('footer_scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.0/TweenMax.min.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.2/underscore-min.js'></script>
<script>
// Get the video
var video = document.getElementById("myVideo");

// Get the button
var btn = document.getElementById("myBtn");

// Pause and play the video, and change the button text
function myFunction() {
  if (video.paused) {
    video.play();
  } else {
    video.pause();
  }
}

setInterval(function(){ getContractStatus() }, 3000);

function getContractStatus() {
    $.ajax({
        method:"POST",
        data:{},
        url:"{{ route('check_contract') }}",
        success:function(res) {
            if(res) {
                var result = JSON.parse(res)
                console.log(result,"msg")
                if(result.length > 0) {
                  btn.click()
                  setTimeout(function(){ show_notify(result); }, 12000);
                }
            }
        }
    })
}

function show_notify(result) {
  console.log(result,"notifi")
	  reset();
    myFunction()
    var cong_text_list = "";
    for(var i=0; i<result.length;i++) {
      cong_text_list += "<h1>Félicitation ! "+result[i].username+" à créer un contrat</h1>";
        // toastr.info("<h1>"+result[i].username + " created new Contract!</h1>", {
        //     positionClass: "toast-top-center",
        //     containerId: "toast-top-center",
        //     timeOut: 0
        // })
        // toastr.info(result[i].username + " created new Contract!", {
        //     timeOut: "10000"
        // })
    }
    $('.congrats').prepend(cong_text_list)
    animateText();
	  animateBlobs();
    setTimeout(function(){ $(".congrats").html("") }, 10000);
}

 
// Click "Congratulations!" to play animation
$(function() {
	var numberOfStars = 20;
	
	for (var i = 0; i < numberOfStars; i++) {
	  $('.congrats').append('<div class="blob fa fa-star ' + i + '"></div>');
	}	

	animateText();
	
	animateBlobs();
});

$('.congrats').click(function() {
	reset();
	
	animateText();
	
	animateBlobs();
});

function reset() {
	$.each($('.blob'), function(i) {
		TweenMax.set($(this), { x: 0, y: 0, opacity: 1 });
	});
	
	TweenMax.set($('h1'), { scale: 1, opacity: 1, rotation: 0 });
}

function animateText() {
		TweenMax.from($('h1'), 0.8, {
		scale: 0.4,
		opacity: 0,
		rotation: 15,
		ease: Back.easeOut.config(4),
	});
}
	
function animateBlobs() {
	
	var xSeed = _.random(350, 380);
	var ySeed = _.random(120, 170);
	
	$.each($('.blob'), function(i) {
		var $blob = $(this);
		var speed = _.random(1, 5);
		var rotation = _.random(5, 100);
		var scale = _.random(0.8, 1.5);
		var x = _.random(-xSeed, xSeed);
		var y = _.random(-ySeed, ySeed);

		TweenMax.to($blob, speed, {
			x: x,
			y: y,
			ease: Power1.easeOut,
			opacity: 0,
			rotation: rotation,
			scale: scale,
			onStartParams: [$blob],
			onStart: function($element) {
				$element.css('display', 'block');
			},
			onCompleteParams: [$blob],
			onComplete: function($element) {
				$element.css('display', 'none');
			}
		});
	});
}</script>
@stop
