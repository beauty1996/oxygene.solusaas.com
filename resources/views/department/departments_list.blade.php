@extends('layouts.default')
{{-- Page title --}}
@section('title')
Advanced Data Tables @parent
@stop
{{-- page level styles --}}
@section('header_styles')
<!-- page vendors -->
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css"> -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.bootstrap4.min.css">
<!--end of page vendors -->
@stop
@section('content')

<!-- content start-->
<section class="content">
    <div class="row">
        <div class="col-lg-12 my-3">
            <div class="card panel-info filterable">
                <div class="card-body table-responsive table-responsive-lg table-responsive-md table-responsive-sm">
                    <table class="table table-bordered table-striped" id="table1" width="100%">
                        <thead>
                            <tr>
                                <th>Id</id>
                                <th>Name</th>
                                <th>Supervisors</th>
                                <th>
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        @if(count($departments_list) > 0)
                            @foreach($departments_list as $temp)
                            <tr>
                                <td>{{$temp->id}}</td>
                                <td>{{$temp->name}}</td>
                                <td>
                                    <?php
                                        $supervisors = $temp->supervisors;
                                        foreach($supervisors as $temp_sup) {
                                            echo "<p>".\App\Models\User::get_username($temp_sup->user_id)."</p>";
                                        }
                                    ?>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-dark btn-sm dropdown-toggle dropdown-toggle-split" id="dropdownMenuReference1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-reference="parent">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuReference1">
                                            <a class="dropdown-item" href="{{route('department_detail', $temp->id)}}">Détails</a>
                                            <a class="dropdown-item" href="{{route('department_edit', $temp->id)}}">Modifier</a>
                                            <a class="dropdown-item" onClick="if(confirm('Are you sure?')) window.location.href= '{{ route("department_delete", $temp->id) }}'">Supprimer</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</section>
<!-- content end-->


@stop
@section('footer_scripts')
<!--   page level js ----------->
<!-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.bootstrap4.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.colVis.min.js"></script>
<script>

$(document).ready(function() {
    var table = $('#table1').DataTable( {
        // lengthChange: false,        
        dom: 'Blfrtip',
        buttons: [ 
            {
                extend: 'csv',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                }
            },
            {
                extend: 'excel',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                }
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 5 ]
                }
            },
            'colvis'
        ]
    } );
 
    table.buttons().container()
        .appendTo( '#table1_wrapper .col-md-6:eq(0)' );
} );
</script>
@stop
