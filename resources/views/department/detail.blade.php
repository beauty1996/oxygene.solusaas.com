@extends('layouts.default')
{{-- Page title --}}
@section('title')
Cards @parent
@stop
{{-- page level styles --}}
@section('header_styles')
<!-- page vendors -->
<link href="{{ asset('css/pages.css')}}" rel="stylesheet">
<!-- select vendors -->
<link href="{{ asset('vendors/select2/css/select2.min.css') }}" rel="stylesheet" />
<link href="{{ asset('vendors/select2/css/select2-bootstrap4.min.css')}}" rel="stylesheet">
<!--end of page vendors -->
@stop
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">

    <div aria-label="breadcrumb" class="card-breadcrumb">
        <h1>Department Detail</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('departments_list')}}">Departments</a></li>
            <li class="breadcrumb-item active" aria-current="page">Detail</li>
        </ol>
    </div>
    <div class="separator-breadcrumb border-top"></div>
</section>
<!-- /.content -->
<section class="content">
    <div class="card p-0">
        <div class="form-horizontal" method="POST" action="{{route('department_save')}}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row mt-3 mb-3 ml-3 mr-3">
                <div class="col-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label><b>Name</b></label>
                        <p>{{ $department->name }}</p>
                    </div>
                </div>
                <div class="col-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label><b>Supervisor</b></label>
                        @foreach($super_list as $temp)
                           @if(in_array($temp->id, $cur_super_list))
                            <p>{{$temp->name}} </p>
                           @endif 
                        @endforeach
                    </div>
                </div>

            </div>
            
        </div>
    </div>
</section>

@stop
@section('footer_scripts')
<!--   page level js ----------->

<script>
// $(document).ready(function() {
//     $(".js-example-templating").select2({
//         templateResult: formatState
//     });
// });
</script>
<!-- end of page level js -->
@stop
