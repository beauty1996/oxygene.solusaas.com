@extends('layouts.default')
{{-- Page title --}}
@section('title')
Cards @parent
@stop
{{-- page level styles --}}
@section('header_styles')
<!-- page vendors -->
<link href="{{ asset('css/pages.css')}}" rel="stylesheet">
<!-- select vendors -->
<link href="{{ asset('vendors/select2/css/select2.min.css') }}" rel="stylesheet" />
<link href="{{ asset('vendors/select2/css/select2-bootstrap4.min.css')}}" rel="stylesheet">
<!--end of page vendors -->
@stop
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">

    <div aria-label="breadcrumb" class="card-breadcrumb">
        <h1>Edit Department</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('departments_list')}}">Departments</a></li>
            <li class="breadcrumb-item active" aria-current="page">Edit</li>
        </ol>
    </div>
    <div class="separator-breadcrumb border-top"></div>
</section>
<!-- /.content -->
<section class="content">
    <div class="card p-0">
        <form class="form-horizontal" method="POST" action="{{route('department_update', $department->id)}}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row mt-3 mb-3 ml-3 mr-3">
                <div class="col-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="name" class="form-control" value="{{ $department->name }}" required />
                    </div>
                </div>
                
                <div class="col-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label>Supervisor</label>
                        <select multiple name="supervisor_list[]" placeholder="Select Supervisor" data-allow-clear="1" required>
                            @foreach($super_list as $temp)
                            <option value="{{$temp->id}}" {{ in_array($temp->id, $cur_super_list) ? "selected":'' }}>{{$temp->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

            </div>
            <div class="row mt-3 mb-3 ml-3 mr-3">
                <button type="submit" class="btn btn-success"> Save</button>
                <button type="reset" class="btn btn-warning hidden-xs" value="Reset">Reset</button>
            </div>
        </form>
    </div>
</section>

@stop
@section('footer_scripts')
<!--   page level js ----------->
<script src="{{ asset('vendors/select2/js/select2.min.js') }}"></script>
<script type="text/javascript">
    $(function () { 
        $('select').each(function () {
            $(this).select2({
                theme: 'bootstrap4',
                width: 'style',
                placeholder: $(this).attr('placeholder'),
                allowClear: Boolean($(this).data('allow-clear')),
            });
        });
    });
</script>

<script>
// $(document).ready(function() {
//     $(".js-example-templating").select2({
//         templateResult: formatState
//     });
// });
</script>
<!-- end of page level js -->
@stop
