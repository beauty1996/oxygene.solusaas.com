@extends('layouts.default')
{{-- Page title --}}
@section('title')
Cards @parent
@stop
{{-- page level styles --}}
@section('header_styles')
<!-- page vendors -->
<link href="{{ asset('css/pages.css')}}" rel="stylesheet">
<!-- select vendors -->
<link href="{{ asset('vendors/select2/css/select2.min.css') }}" rel="stylesheet" />
<link href="{{ asset('vendors/select2/css/select2-bootstrap4.min.css')}}" rel="stylesheet">
<!--end of page vendors -->
@stop
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">

    <div aria-label="breadcrumb" class="card-breadcrumb">
        <h1>User Detail</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('users_list')}}">users</a></li>
            <li class="breadcrumb-item active" aria-current="page">Detail</li>
        </ol>
    </div>
    <div class="separator-breadcrumb border-top"></div>
</section>
<section class="content">
    <div class="card p-0">
        <form class="form-horizontal" method="POST" action="{{route('user_update', $user->id)}}" enctype="multipart/form-data">
        {{ csrf_field() }}
            <input type="hidden" name="user_id" value="{{ $user->id}}">
            <div class="row mt-3 mb-3 ml-3 mr-3">
                <div class="col-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label>Name</label>
                        <p>{{$user->name}}</p>
                    </div>
                    <div class="form-group">
                        <label>Address</label>
                        <p>{{$user->address}}</p>
                    </div>
                </div>

                <div class="col-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label>Email</label>
                        <p>{{$user->email}}</p>
                    </div>
                    <div class="form-group">
                        <label>N° de Téléphone</label>
                        <p>{{$user->telephone}}</p>
                    </div>
                </div>

                <div class="col-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label>Role</label>
                        <p>{{$user->role->name}}</p>
                    </div>
                    
                    @if($user->role_id != 2)
                    <div class="form-group">
                        <label>Department</label>
                        <p>{{$user->department ? $user->department->name : ""}}</p>
                    </div>
                    @endif
                </div>

            </div>
        </form>
    </div>
</section>

@stop
@section('footer_scripts')
<!--   page level js ----------->
<script src="{{ asset('vendors/select2/js/select2.min.js') }}"></script>
<script type="text/javascript">
    $(function () { 
        $('select').each(function () {
            $(this).select2({
                theme: 'bootstrap4',
                width: 'style',
                placeholder: $(this).attr('placeholder'),
                allowClear: Boolean($(this).data('allow-clear')),
            });
        });
    });
</script>

<script>
// $(document).ready(function() {
//     $(".js-example-templating").select2({
//         templateResult: formatState
//     });
// });
</script>
<!-- end of page level js -->
@stop
