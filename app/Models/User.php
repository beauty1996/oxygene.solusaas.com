<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role() {
        return $this->belongsTo(Role::class, 'role_id','id');
    }

    public function departments() {
        return $this->hasMany(DepartmentUser::class);
    }

    public static function contracts() {
        $_this = new self;
        return $_this->hasMany(Contract::class, 'created_by', 'id');
    }

    public static function team_users($user_id) {
        $user_list = [];
        $user_deps = DepartmentUser::where('user_id', $user_id)->pluck('department_id')->toArray();
        if(count($user_deps) > 0)
            $user_list = DepartmentUser::whereIn('department_id', $user_deps)->pluck('user_id')->toArray();
        
        return $user_list;
    }

    public static function get_username($user_id) {
        $user = User::find($user_id);
        if($user)
            return $user->name;
        else
            return "No User";
    }
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
