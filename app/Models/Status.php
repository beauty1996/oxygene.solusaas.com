<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    use HasFactory;

    public function contracts_list() {
        return $this->hasMany(Contract::class, 'd’appel', 'id');
    }
    public function financial_contracts_list() {
        return $this->hasMany(Contract::class, 'Financier', 'id');
    }
    public static function get_User_contract() {
        $user = auth()->user();
        // $_this = new self;
        // $list = $_this->contracts_list();
        $list = User::contracts();
        
        if(count($list->get()) == 0 ){
            return null;
        }

        if($user->role_id == 1) {   //admin
            return $list;
        }elseif($user->role_id == 2) {  //supervisor
            $cur_dep = DepartmentUser::where('user_id', $user->id)->get();
            if(count($cur_dep) > 0) {
                $group_users = DepartmentUser::where('department_id', $cur_dep->department_id)->pluck('user_id')->toArray();
                return $list->whereIn('created_by', $group_users)->get();
            }
            return null;
        }
        elseif($user->role_id == 3) {   //agent
            return $list->where('created_by', $user->id);
        }
    }
}