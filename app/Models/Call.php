<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Call extends Model
{
    use HasFactory;

    public function upload_user(){
        return $this->belongsTo(User::class, 'uploaded_by','id');
    }
}
