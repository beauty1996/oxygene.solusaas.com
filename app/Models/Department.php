<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use HasFactory;

    public function supervisors() {
        return $this->hasMany(DepartmentUser::class, 'department_id', 'id');
    }

    public static function get_dep_name($dep_id) {
        $dep = Department::find($dep_id);
        if(empty($dep->name))
            return "Not Found";
        else {
            return $dep->name;
        }
    }
}
