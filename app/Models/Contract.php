<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    use HasFactory;
    protected $fillable = [
        'Créateur',
        'saisie',
        'Nom',
        'Prénom',
        'naissance',
        'Téléphone',
        'email',
        'd’appel',
        'Postal',
        'Vendu',
        'Mensuelle',
        'Financier',
        'CALL1',
        'CALL1',
        'Comments',
        'Historique'
    ];

    public function user(){
        return $this->belongsTo(User::class, 'created_by','id');
    }

    public function dStatus() {
        return $this->belongsTo(Status::class, 'd’appel', 'id');
    }

    public function fStatus() {
        return $this->belongsTo(Status::class, 'Financier', 'id');
    }

    public function canDelete($contract_id) {
        if(auth()->user()->role_id == 1)
            return true;
        elseif(auth()->user()->role_id == 2) {
            $contract = Contract::find($contract_id);
            if($contract) {
                $contract_creator_id = $contract->created_by;
                if($contract_creator_id == 1)
                    return true;
                else {
                    $current_user = auth()->user()->id;
                    $current_dep = DepartmentUser::where('user_id', $current_user)->pluck('department_id')->toArray();
                    $current_dep_users = DepartmentUser::whereIn('department_id', $current_dep)->get()->toArray();
                    $team_contracts = Contract::whereIn('created_by', $current_dep_users)->get()->toArray();
                    
                    if(in_array($contract_id, $team_contracts))
                        return true;
                    else
                        return false;
                }
            }
        }elseif(auth()->user()->role_id == 3)
            return false;

        return false;
    }
}
