<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Department;
use App\Models\DepartmentUser;
use App\Models\User;
use App\Models\Role;
use App\Helpers\Helper;
use Session;

class DepartmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\departments\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function new_department(Request $request) {
        if(!in_array(auth()->user()->role_id, [1])){
            return back();
        }
        $department_list = Department::all();
        $super_list      = User::where('role_id', 2)->get();
        return view('department/new_department', compact('department_list', 'super_list'));
    }

    public function departments_list() {
        if(!in_array(auth()->user()->role_id, [1])){
            return back();
        }
        $departments_list = department::all();
        return view('department/departments_list')->with('departments_list', $departments_list);
    }

    public function department_edit($id) {
        if(!in_array(auth()->user()->role_id, [1])){
            return back();
        }
        $department = department::find($id);
        $role_list = Role::where('name', '!=', 'Admin')->get();
        $department_list = Department::all();
        $dep_super_list  = DepartmentUser::where('department_id', $id)->where('option', 2)->pluck('user_id')->toArray();
        $cur_super_list  = User::whereIn('role_id', $dep_super_list)->pluck('id')->toArray();
        $super_list      = User::where('role_id', 2)->get();
        
        return view('department/edit')->with('department', $department)->with('cur_super_list', $dep_super_list)
        ->with('super_list', $super_list)->with('role_list', $role_list)->with('department_list', $department_list);
    }

    public function department_detail($id) {
        if(!in_array(auth()->user()->role_id, [1])){
            return back();
        }
        $department = department::find($id);
        $role_list = Role::where('name', '!=', 'Admin')->get();
        $department_list = Department::all();
        $dep_super_list  = DepartmentUser::where('department_id', $department->id)->where('option', 2)->pluck('user_id');
        $cur_super_list  = User::whereIn('id', $dep_super_list)->pluck('id')->toArray();
        $super_list      = User::where('role_id', 2)->get();

        return view('department/detail')->with('department', $department)->with('cur_super_list', $cur_super_list)
        ->with('super_list', $super_list)->with('role_list', $role_list)->with('department_list', $department_list);
    }

    public function department_save(Request $request) {
        if(!in_array(auth()->user()->role_id, [1])){
            return back();
        }
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255', 'unique:departments']
        ]);

        $new_department = new Department();

        if($department_id = $this->store($new_department, $request) > 0) {
            $message = "department saved Successfully";
            if(count($request->supervisor_list) > 0) {
                foreach($request->supervisor_list as $temp) {
                    $dep_user = new DepartmentUser();
                    $dep_user->department_id = $department_id;
                    $dep_user->user_id = $temp;
                    $dep_user->option  = 2;
                    $dep_user->save();
                }
            }
        }
        else
            $message = "There was something wrong";

        Session::flash('flash_message', __($message));
        return redirect()->route('departments_list');
    }

    public function department_delete($id) {
        if(!in_array(auth()->user()->role_id, [1])){
            return back();
        }
        if($id == 1) {
            Session::flash('flash_message', 'You can not delete default Department');
            return back();
        }
        if($department = department::find($id)) {
            $dep_users_list = DepartmentUser::where('department_id', $id)->get();

            foreach($dep_users_list as $temp_dep_user) {
                $temp_dep_user->department_id = 1;
                $temp_dep_user->save();
            }
            
            $department->delete();
            $message = "department deleted Successfully";
        }
        else
            $message = "There was something wrong";

        Session::flash('flash_message', __($message));
        return back();
    }

    public function department_update(Request $request, $department_id) {
        if(!in_array(auth()->user()->role_id, [1])){
            return back();
        }
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255']
        ]);

        if ($validator->fails()) {
            Session::flash('flash_message', $validator->messages()->first());
            return redirect()->back()->withInput();
        }
        
        $update_department = Department::find($department_id);
        
        if($this->store($update_department, $request) > 0) {
            $message = "department updated Successfully";
            $department_id = $this->store($update_department, $request);
            if(count($request->supervisor_list) > 0) {
                $old_dep_users = DepartmentUser::where('department_id', $department_id)->where('option', 2);
                
                if(count($old_dep_users->get()) > 0)
                    $old_dep_users->delete();

                foreach($request->supervisor_list as $temp) {
                    $check_exist = DepartmentUser::where('user_id', $temp)->where('department_id', $department_id)->first();
                    if(empty($check_exist)) {
                        $dep_user = new DepartmentUser();
                        $dep_user->department_id = $department_id;
                        $dep_user->user_id = $temp;
                        $dep_user->option  = 2;
                        $dep_user->save();
                    }
                }
            }
        }
        else
            $message = "There was something wrong";
        
        Session::flash('flash_message', __($message));
        return back();
    }

    public function store($department, $request) {
        $department->name      = $request->name;

        if($department->save())
            return $department->id;
        else
            return 0;
    }
}