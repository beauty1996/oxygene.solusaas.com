<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\Role;
use App\Models\Department;
use App\Models\DepartmentUser;
use App\Models\Contract;
use Session;
use App\Helpers\Helper;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\users\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function new_user(Request $request) {
        if(!in_array(auth()->user()->role_id, [1])){
            return back();
        }
        $role_list = Role::all();
        $department_list = Department::all();
        return view('user/new_user', compact('role_list', 'department_list'));
    }

    public function user_save(Request $request) {
        if(!in_array(auth()->user()->role_id, [1])){
            return back();
        }
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'role' => ['required'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'telephone' => ['required','numeric','max:100000000000'],
            'password' => ['required', 'string', 'min:8', 'confirmed']
        ]);

        if(in_array($request->role, [2, 3]) && empty($request->department)){
            Session::flash('flash_message', "Please select department!");
            return back();
        }

        if ($validator->fails()) {
             Session::flash('flash_message', $validator->messages()->first());
             return redirect()->back()->withInput();
        }
        $new_user = new User();
        
        if(!empty($this->store($new_user, $request))) {
            $message = "User saved Successfully";
            $user = $this->store($new_user, $request);
            if(in_array($user->role_id, [2,3])) {
                $department_user = new DepartmentUser();
                $department_user->user_id = $user->id;
                $department_user->department_id = $request->department;
                $department_user->save();
            }
        }
        else
            $message = "There was something wrong";

        Session::flash('flash_message', __($message));
        return redirect('users_list');
    }

    public function users_list() {
        if(!in_array(auth()->user()->role_id, [1])){
            return back();
        }
        if(!in_array(auth()->user()->role_id, [1])){
            return back();
        }
        Helper::can_access('new_department', auth()->user()->role_id);
        $users_list = User::all();
        return view('user/users_list')->with('users_list', $users_list);
    }

    public function user_edit($id) {
        if(!in_array(auth()->user()->role_id, [1])){
            return back();
        }
        $user = user::find($id);
        $role_list = Role::all();
        $department_list = Department::all();
        return view('user/edit')->with('user', $user)->with('role_list', $role_list)->with('department_list', $department_list);
    }

    public function user_detail($id) {
        if(!in_array(auth()->user()->role_id, [1])){
            return back();
        }
        $user = user::find($id);
        $role_list = Role::all();
        $department_list = Department::all();
        return view('user/detail')->with('user', $user)->with('role_list', $role_list)->with('department_list', $department_list);
    }

    public function user_delete($id) {
        if(!in_array(auth()->user()->role_id, [1])){
            return back();
        }
        if($id == 1) {
            Session::flash('flash_message', 'You can not delete Admin');
            return back();
        }
        if($user = User::find($id)) {
            $contracts = Contract::where('created_by', $id)->get();
            foreach($contracts as $temp) {
                $temp->created_by = 1;
                $temp->save();
            }
            //department user
            $dep_user = DepartmentUser::where('user_id', $id)->get();
            $user->delete();
            $message = "User deleted Successfully";
        }
        else
            $message = "There was something wrong";

        Session::flash('flash_message', __($message));
        return back();
    }

    public function user_update(Request $request) {
        if(!in_array(auth()->user()->role_id, [1])){
            return back();
        }
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'role' => ['required'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'telephone' => ['required','numeric','max:100000000000']
        ]);
        if(!$validator->fails() && in_array($request->role, [3])) {
            $validator = Validator::make($request->all(), [
                'department' => ['required', 'string']
            ]);
        }
        
        if ($validator->fails()) {
             Session::flash('flash_message', $validator->messages()->first());
             return redirect()->back()->withInput();
        }
        
        $update_user = user::find($request->user_id);
        
        if(!empty($this->store($update_user, $request))) {
            $message = "User updated Successfully";
            if($update_user->role_id == 3) {
                $department_user = DepartmentUser::where('user_id', $update_user->id)->first();
                $department_user->user_id = $update_user->id;
                $department_user->department_id = $request->department;
                $department_user->save();
            }
        }
        else
            $message = "There was something wrong";
        
        Session::flash('flash_message', __($message));
        return back();
    }

    public function store($user, $request) {
        if(empty($user))
            $user->created_by = auth()->user()->id;
        $user->name      = $request->name;
        $user->email     = $request->email;
        $user->address   = $request->address;
        $user->telephone = $request->telephone;
        $user->password  = $request->password ? bcrypt($request->password) : $user->password;
        $user->role_id   = $request->role;

        if(in_array($request->role, [2, 3])) {
            $user->department_id   = $request->department;
        }
        
        if($user->save())
            return $user;
        else
            return 0;
    }
}