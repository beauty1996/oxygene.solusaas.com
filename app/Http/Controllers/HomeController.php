<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Contract;
use App\Models\Call;
use App\Models\Status;
use App\Models\User;
use App\Models\ContractStatus;
use App\Helpers\Helper;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return redirect('dashboard');
        // return view('home');
    }

    public function dashboard(Request $request) {
        $data = $this->get_static();
        return view('index', compact('data'));
    }
    public function get_static() {
        $user_role = auth()->user()->role_id;
        $statuses = Status::all();
        // $contracts_list = Status::get_User_contract();
        $result = [];
        $t_chart[] = 'name';
        $t_chart[] = 'count';
        $chart_data[] = $t_chart;
        $financial_chart_data[] = $t_chart;
        foreach($statuses as $temp) {
            $temp_c  = [];
            if($temp->option == "dapple")
                $contracts = $temp->contracts_list;
            else    
                $contracts = $temp->financial_contracts_list;

            if(in_array($user_role, [1, 4, 5])) {
                $temp['count'] = count($contracts);
            }elseif($user_role == 2) {
                $i = 0;
                $users_list = User::team_users(auth()->user()->id);
                
                foreach($contracts as $temp_cont) {
                    if(in_array($temp_cont['created_by'], $users_list)) {
                        $i++;
                    }
                }
                $temp['count'] = $i;
            }elseif($user_role == 3) {
                $i = 0;

                foreach($contracts as $temp_cont_user) {
                    if($temp_cont_user['created_by'] == auth()->user()->id) {
                        $i++;
                    }
                }
                $temp['count'] = $i;
            }
            
            $temp_c[] = $temp['name'];
            $temp_c[] = $temp['count'];
            $result[] = $temp_c;
            $total_result[] = $temp;
            
            if($temp['option'] == "dapple")
                $chart_data[] = $temp_c;
            else
                $financial_chart_data[] = $temp_c;
        }
        
        $p_chart[] = 'name';
        $p_chart[] = 'count';
        $product_data[] = $p_chart;
        $product_status = ['IJH', 'GAV', 'PJ', 'DCA', 'ASSISTANCE'];
        $all_contracts = Contract::all();
        foreach($product_status as $pro_temp) {
            $p_temp = [];
            $p_temp[] = $pro_temp;
            $i=0;
            foreach($all_contracts as $temp_contract){
                if(in_array($pro_temp, json_decode($temp_contract->Vendu)))
                    $i++;
            }
            $p_temp[] = $i;
            $product_data[] = $p_temp;
        }
        
        $data['total'] = $total_result;
        $data['dapple'] = $chart_data;
        $data['financial'] = $financial_chart_data;
        $data['product'] = $product_data;
        return $data;
    }
    public function new_contract(Request $request) {
        $statuses = Status::all();
        return view('contract/new_contract')->with('statuses', $statuses);
    }

    public function contract_save(Request $request) {
        $validator = Validator::make($request->all(), [
            'Téléphone' => ['required', 'string', 'max:255', 'unique:contracts']
        ]);

        if ($validator->fails()) {
            Session::flash('flash_message', $validator->messages()->first());
            return redirect()->back()->withInput();
        }
        
        $new_contract = new Contract();
        if($this->store($new_contract, $request))
            $message = "Contract saved Successfully";
        else
            $message = "There was something wrong";

        Session::flash('flash_message', __($message));
        if(auth()->user()->role_id == 3)
            return redirect()->route('dashboard');
        else
            return redirect()->route('contracts_list');
    }

    public function contracts_list() {
        if(!in_array(auth()->user()->role_id, [1,2,4,5])){
            return back();
        }
        $user = auth()->user();
        if(in_array($user->role_id, [1, 4, 5])) {
            $contracts_list = Contract::all();
        }
        else{
            $team_users = User::team_users($user->id);
            if(!is_array($team_users)) 
                $team_users[] = $team_users;
            $contracts_list = Contract::whereIn('created_by', $team_users)->get();
        }

        return view('contract/contracts_list')->with('contracts_list', $contracts_list);
    }

    public function contract_edit($id) {
        if(!in_array(auth()->user()->role_id, [1,2,4,5])){
            return back();
        }
        $statuses   = Status::all();
        $contract   = Contract::find($id);
        $call_files = Call::where('contract_id', $id)->get();
        $change_status = ContractStatus::where('contract_id', $id)->get();
        return view('contract/edit', compact('contract', 'call_files', 'statuses', 'change_status'));
    }

    public function contract_detail($id) {
        if(!in_array(auth()->user()->role_id, [1,2,4,5])){
            return back();
        }
        $statuses   = Status::all();
        $contract   = Contract::find($id);
        $call_files = Call::where('contract_id', $id)->get();
        $change_status = ContractStatus::where('contract_id', $id)->get();
        return view('contract/detail', compact('contract', 'call_files', 'statuses', 'change_status'));
    }

    public function contract_delete($id) {
        if(!in_array(auth()->user()->role_id, [1,2,4,5])){
            return back();
        }
        
        if($contract = Contract::find($id)) {
            $contract->delete();
            $message = "Contract deleted Successfully";
        }
        else
            $message = "There was something wrong";

        Session::flash('flash_message', __($message));
        return back();
    }

    public function contract_update(Request $request) {
        if(!in_array(auth()->user()->role_id, [1,2,4,5])){
            return back();
        }
        Helper::can_access('contract_update', auth()->user()->role_id);
        $new_contract = Contract::find($request->contract_id);
        if($this->store($new_contract, $request))
            $message = "Contract updated Successfully";
        else
            $message = "There was something wrong";

        Session::flash('flash_message', __($message));
        return back();
    }

    public function store($new_contract, $request) {
        if(empty($new_contract->id))
            $new_contract->created_by = auth()->user()->id;
        // check if gestionair update the contract status
        if((auth()->user()->role_id == 4) && !empty($new_contract->d’appel) && ($new_contract->d’appel != $request->d’appel))
            $this->gentionair_status_update($new_contract->id);

        if((auth()->user()->role_id == 5) && !empty($new_contract->Financier) && ($new_contract->Financier != $request->Financier))
            $this->res_gentionair_status_update($new_contract->id);

        if(!empty($request->d’appel))
            $new_contract->d’appel  = $request->d’appel;

        $new_contract->Nom  = $request->Nom;
        $new_contract->Prénom  = $request->Prénom;
        $new_contract->naissance  = $request->naissance;
        $new_contract->Téléphone  = $request->Téléphone;
        $new_contract->email   = $request->email;
        $new_contract->Postal  = $request->Postal;
        $new_contract->Vendu  = is_array($request->Vendu) ? json_encode($request->Vendu) : json_encode([$request->Vendu]);
        $new_contract->Mensuelle  = $request->Mensuelle;
        $new_contract->Financier  = $request->Financier;
        $new_contract->Comments  = $request->Comments;

      
        if($new_contract->save())
            return true;
        else
            return false;
    }

    public function contract_call(Request $request, $option, $contract_id)
    {
        $call_fileName = "";
        if($request->hasfile('file')) {
            $temp = $request->file('file');
            $path = $_FILES['file']['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            $call_fileName = $temp->getClientOriginalName();
            $temp->move(public_path('uploads/call_files'), $call_fileName);
            
            if($ext == "aac" || $ext == "mp3") {
                $type = "audio";
            }elseif($ext == "jpg" || $ext == "png" || $ext == "jpeg")
                $type = "image";
            else {
                $type = "other";
            }

            $call_file = new Call();
            $call_file->name        = $call_fileName;
            $call_file->file_path   = '/uploads/call_files/'.$call_fileName;
            $call_file->option      = $option;
            $call_file->contract_id = $contract_id;
            $call_file->uploaded_by = auth()->user()->id;
            $call_file->ext         = $ext;
            $call_file->type        = $type;
            
            if($call_file->save()) {
                if($option == 1 && auth()->user()->role_id == 2) {
                    $contract = Contract::find($contract_id);
                    $contract->d’appel = 2;
                    $contract->save();
                }
            }
            
        }
        return response()->json(['success'=> $call_fileName]);
    }

    public function gentionair_status_update($contract_id) 
    {
        $contract_statuses = new ContractStatus();
        $contract_statuses->contract_id = $contract_id;
        $contract_statuses->updated_by = auth()->user()->id;
        $contract_statuses->option = 4;
        $contract_statuses->save();
    }
    public function res_gentionair_status_update($contract_id) 
    {
        $contract_statuses = new ContractStatus();
        $contract_statuses->contract_id = $contract_id;
        $contract_statuses->updated_by = auth()->user()->id;
        $contract_statuses->option = 5;
        $contract_statuses->save();
    }
    
    public function animation() {
        if(in_array(auth()->user()->role_id ,[1, 6]))
            return view('animation/index');
        else
            return back();
    }

    public function check_contract() {
        $contracts = Contract::select('contracts.*', 'users.name as username')->where('contracts.checked', 0)->leftjoin('users', 'users.id', 'contracts.created_by')->get();
        $new_contracts = $contracts;
        foreach($contracts as $temp) {
            $temp->checked = 1;
            $temp->save();
        }
        return json_encode($new_contracts);
    }

    public function call_delete(Request $request, $call_id) {
        if(!in_array(auth()->user()->role_id, [1])){
            return back();
        }
        $call_file = Call::find($call_id);
        $call_file->is_deleted = 1;
        if($call_file->save())
            $message = "Call file deleted Successfully";
        else
            $message = "There was something wrong";

        Session::flash('flash_message', __($message));
        return back();
    }
}
