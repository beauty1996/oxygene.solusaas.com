<?php 

   namespace App\Helpers;

    use Hash;

    use App\Admin;

    use App\User;

    use Auth;

    use App\Requests;

    use Mail;

    use File;

    use Log;

    use Storage;

    use Setting;

    use DB;

    class Helper
    {

        /**
         * Used to generate index.php
         *
         * 
         */

        public static function can_access($target, $role_id) {
            if(in_array($target, 
            ["new_department", "departments_list", "department_edit", "department_detail", 
            "department_delete", "department_save", "department_update"]
            )) {
                switch($role_id) {
                    case 1:
                        return true;
                    break;
                    default:
                        return false;
                    break;
                }
            }
            elseif($target == "contracts_list") {
                switch($role_id) {
                    case 1: 
                        return true;
                    break;
                    default:
                        return false;
                    break;
                }
            }elseif($target == "contract_edit") {
                switch($role_id) {
                    case 1: 
                        return true;
                    break;
                    default:
                        return false;
                    break;
                }
            }elseif($target == "contract_detail") {
                switch($role_id) {
                    case 1: 
                        return true;
                    break;
                    default:
                        return false;
                    break;
                }
            }elseif($target == "contract_delete") {
                switch($role_id) {
                    case 1: 
                        return true;
                    break;
                    default:
                        return false;
                    break;
                }
            }elseif($target == "contract_update") {
                switch($role_id) {
                    case 1: 
                        return true;
                    break;
                    default:
                        return false;
                    break;
                }
            }

        }
    }



